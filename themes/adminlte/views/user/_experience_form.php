<div class="table-responsive">
  <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered">
          <tr>
    <td colspan="2"><strong>TAHUN</strong></td>
    <td colspan="2"><strong>SEKOLAH/INSTITUSI</strong></td>
    <td colspan="2"><strong>KAEDAH YANG DIGUNAKAN</strong></td>
  </tr>
  <tr>
    <td colspan="2"><?php echo $form->textField($academic,'exp_1_year',array('class'=>'span1','maxlength'=>4)); ?></td>
    <td colspan="2"><?php echo $form->textField($academic,'exp_1_school',array('class'=>'span3')); ?></td>
    <td colspan="2"><?php echo $form->textField($academic,'exp_1_method',array('class'=>'span3')); ?></td>
  </tr>
 <tr>
    <td colspan="2"><?php echo $form->textField($academic,'exp_2_year',array('class'=>'span1','maxlength'=>4)); ?></td>
    <td colspan="2"><?php echo $form->textField($academic,'exp_2_school',array('class'=>'span3')); ?></td>
    <td colspan="2"><?php echo $form->textField($academic,'exp_2_method',array('class'=>'span3')); ?></td>
  </tr>
  <tr>
    <td colspan="2"><?php echo $form->textField($academic,'exp_3_year',array('class'=>'span1','maxlength'=>4)); ?></td>
    <td colspan="2"><?php echo $form->textField($academic,'exp_3_school',array('class'=>'span3')); ?></td>
    <td colspan="2"><?php echo $form->textField($academic,'exp_3_method',array('class'=>'span3')); ?></td>
  </tr>
  </table>
</div>