<?php $this->beginContent('application.views.layouts.main_lte');
//$this->htmlOptions=array('class'=>'brand');
?>
    <div class="container">
        <div class="span2 right">
            <div id="navbar">
                <?php
                $this->beginWidget('zii.widgets.CPortlet', array(
                    'title' => Yii::t('strings', $this->title),
                    'htmlOptions' => array('class' => 'navbar-inner'),
                ));
                $this->widget('zii.widgets.CMenu', array(
                    'items' => $this->menu,
                    'htmlOptions' => array('class' => 'nav'),
                ));


                $this->endWidget();
                ?>
            </div>
            <!-- sidebar -->
        </div>


        <div class="span9 left">
            <div id="content">
                <?php echo $content; ?>
            </div>
            <!-- content -->
        </div>

    </div>
<?php $this->endContent(); ?>