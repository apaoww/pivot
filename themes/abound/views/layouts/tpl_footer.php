	<footer>
        <div class="subnav navbar navbar-fixed-bottom">
            <div class="navbar-inner">
                <div class="container">
                    Copyright © <?php echo date("Y");?> Land and Survey Department, Sarawak.<br>
                    <small>DISCLAIMER: Land and Survey Department Sarawak shall not be liable for any loss or damage caused by the usage of any information obtained from this website.
                    Best viewed in 1024 x 768 using Google Chrome or Mozilla Firefox.</small>
                </div>
            </div>
        </div>      
	</footer>

