<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-lg-5">
        <!-- small box -->
        <div class="box box-danger">
            <div class="box-header">
                <p class="box-title">
                    <?php echo Yii::t('application','Error'); ?>
                </p>

            </div>
            <div class="inner">

                <p class="alert alert-danger" >
                    <?php echo Yii::t('application', 'No employee found') ?>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div><!-- ./col -->
</div><!-- /.row -->