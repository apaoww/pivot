<?php
/* @var $this PrimiumController */
/* @var $model Primium */

$this->breadcrumbs=array(
    'Premium'=>array('index'),
    'Manage',
);

$this->menu=array(
    array('label'=>'List Premium', 'url'=>array('index')),
    array('label'=>'Create Premium', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#primium-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>First Reminder List</h1>





<?php

$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'primium-grid',
    'dataProvider'=>$model->firstReminder(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        'year',
        'dls_reference',
        'date_approve',
        'affected_lot',
        array(
            'name'=>'status',
            'value'=>array($model,'renderStatus'),
        ),
        /*
        'developer',
        'date_mosa_issue',
        'date_mosa_expired',
        'subdivided',
        'premium',qqqqa
        'annual_rent',
        'preparation_title_fee',

        array(
            'class'=>'CButtonColumn',
        ),*/
        array
        (
            'class'=>'CButtonColumn',
            'template'=>'{email}',
            'buttons'=>array
            (
                'email' => array
                (
                    'label'=>'Letter',
                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/envelope_icon_16.png',
                    'url'=>'Yii::app()->createUrl("primium/firstletter", array("id"=>$data->id))',
                ),
            ),
        ),
    ),
)); ?>