<?php
/* @var $this PrimiumController */
/* @var $model Primium */

$this->breadcrumbs=array(
	'Premium'=>array('index'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List Premium', 'url'=>array('index')),
	array('label'=>'Manage Premium', 'url'=>array('admin')),
);
?>

<h1>Create Premium</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>