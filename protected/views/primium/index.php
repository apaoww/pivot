<?php
/* @var $this PrimiumController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Premium',
);

$this->menu=array(
	array('label'=>'Create Premium', 'url'=>array('create')),
	array('label'=>'Manage Premium', 'url'=>array('admin')),
);
?>

<h1>Premium</h1>
