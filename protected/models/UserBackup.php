<?php

/**
 * This is the model class for table "hrm_user".
 *
 * The followings are the available columns in table 'hrm_user':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $last_login_time
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $update_time
 * @property integer $update_user_id
 *
 * The followings are the available model relations:
 * @property Dependent[] $dependents
 * @property EmergencyContact[] $emergencyContacts
 * @property EmployeeCertification[] $employeeCertifications
 * @property EmployeeEducation[] $employeeEducations
 * @property EmployeeLanguage[] $employeeLanguages
 * @property EmployeeSkill[] $employeeSkills
 * @property JobVacancy[] $jobVacancies
 * @property LeaveApplication[] $leaveApplications
 * @property UserProfile[] $userProfiles
 * @property UserProfile[] $userProfiles1
 */
class User extends CommonActiveRecord
{
	const USER_ADMIN=0;
	const USER_STUDENT=1;

	public $new_password;
        public $new_password_repeat;
        public $old_password;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array( 'new_password, new_password_repeat', 'required', 'on' => 'changePassword' ),
            array( 'new_password', 'length', 'max' => 50 ),
            array( 'new_password', 'compare' ),
            array('old_password', 'equalPasswords', 'on'=>'changePassword'),
            // Make new_password_repeat safe. If it doesn't appear in any other rule, it's considered not safe!
            array( 'new_password_repeat', 'safe' ),
			array('username, password, user_type', 'required'),
			array('create_user_id, update_user_id, user_type', 'numerical', 'integerOnly'=>true),
			array('username', 'length', 'max'=>45),
			array('username', 'unique'),
			array('email', 'email'),
			array('password, email', 'length', 'max'=>125),
			array('last_login_time, create_time, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, password, email, last_login_time, create_time, create_user_id, update_time, update_user_id, user_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			
			'userProfile' => array(self::HAS_ONE, 'Profile', 'user_id'),
                        'intakeRegister' => array(self::HAS_MANY, 'IntakeRegister', 'user_id'),
                        'enrolCourse' => array(self::HAS_MANY, 'enrolCourse', 'user_id'),
                        'enrolCourseBy' => array(self::HAS_MANY, 'enrolCourse', 'enrol_by'),
                        
                        
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('modelLabels','ID'),
			'username' => Yii::t('modelLabels','Username/IC'),
			'password' => Yii::t('modelLabels','Password'),
			'email' => Yii::t('modelLabels','Email'),
			'last_login_time' => Yii::t('modelLabels','Last Login Time'),
			'create_time' => Yii::t('modelLabels','Create Time'),
			'create_user_id' => Yii::t('modelLabels','Create User'),
			'update_time' => Yii::t('modelLabels','Update Time'),
			'update_user_id' => Yii::t('modelLabels','Update User'),

			'user_type' => Yii::t('modelLabels','User Type'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('last_login_time',$this->last_login_time,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('create_user_id',$this->create_user_id);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('update_user_id',$this->update_user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	/**
	* Apply a hash on the password before we store it in the database
	*/

	protected function afterValidate()
	{
		parent::afterValidate();
		if(!$this->hasErrors())
			$this->password = $this->hashPassword($this->password);
	}

	/**
	* Generates the password hash
	* @param string password
	* @return string hash
	*/
	public function hashPassword($password)
	{
		return hash('md5',$password);
	}

	/**
	* Check if the given password is correct.
	* @param string the password to be validated
	* @return boolen whether the password is valid
	*/
	public function validatePassword($password)
	{
		return $this->hashPassword($password)===$this->password;
	}
	public function equalPasswords($attribute, $params)
    {
        $user = User::model()->findByPk(Yii::app()->user->id);
        if ($user->password != $this->hashPassword($this->old_password))
        $this->addError($attribute, 'Kata laluan lama tidak tepat.');
    }

    /**
    * Retrieve list of user type
    * @return array of user type
    */
    public function getUserOptions()
    {
    	return array(
    		self::USER_ADMIN=>Yii::t('modelLabels', 'Admin'),
    		self::USER_STUDENT=>Yii::t('modelLabels', 'Student'),
    		);
    }

    /**
    * @return string of current user type
   
    public function getUserText()
    {
    	$userOptions = $this->userOptions;
    	return isset($userOptions[$this->user_type]) ? $userOptions[$this->user_type] : Yii::t('errorMessages', 'Unknown user type.');
    } */
}