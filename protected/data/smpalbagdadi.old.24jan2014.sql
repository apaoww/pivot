SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `albaghda_smpalbaghdadi`.`smpalbaghdadi_profile` DROP FOREIGN KEY `smpalbaghdadi_profile_ibfk_1` ;

ALTER TABLE `albaghda_smpalbaghdadi`.`smpalbaghdadi_academic` DROP COLUMN `user_id` , ADD COLUMN `user_id` INT(11) NOT NULL  AFTER `academic_id` , 
  ADD CONSTRAINT `fk_smpalbaghdadi_academic_smpalbaghdadi_user1`
  FOREIGN KEY (`user_id` )
  REFERENCES `albaghda_smpalbaghdadi`.`smpalbaghdadi_user` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `fk_smpalbaghdadi_academic_smpalbaghdadi_user1_idx` (`user_id` ASC) 
, DROP INDEX `user_id` ;

ALTER TABLE `albaghda_smpalbaghdadi`.`smpalbaghdadi_profile` CHANGE COLUMN `user_id` `user_id` INT(11) NOT NULL DEFAULT NULL COMMENT 'Yii::t(\"modelLabels\", \"User ID\")'  , 
  ADD CONSTRAINT `smpalbaghdadi_profile_ibfk_1`
  FOREIGN KEY (`user_id` )
  REFERENCES `albaghda_smpalbaghdadi`.`smpalbaghdadi_user` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
