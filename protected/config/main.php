<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'timeZone' => 'Asia/Kuala_Lumpur',
    'sourceLanguage' => 'en',
    'language' => 'en',
    'behaviors' => array(
        'onBeginRequest' => array(
            'class' => 'application.components.behaviors.BeginRequest'
        )
    ),
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'theme' => 'abound',
    'name' => "Yii Application Template",

    // preloading 'log' component
    'preload' => array(
        'log',
        'bootstrap',
        'customgii'),

    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.modules.rights.*',
        'application.modules.rights.components.*',
        'application.extensions.debugtoolbar.*',
    ),

    'defaultController' => 'site',

    // application modules
    'modules' => array(
        'rights' => array(
            'debug' => true,
            //'install'=>true,
            'enableBizRuleData' => true,
        ),
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => false,
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => false, //array('enaisolutions.com','::1'),
            'generatorPaths' => array(
                'application.gii.generators'

            ),
        ),
    ),

    // application components
    'components' => array(
        'localtime' => array(
            'class' => 'LocalTime',
        ),
        'user' => array(
            'class' => 'RWebUser',
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),

        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=pivot',
            'emulatePrepare' => true,
            'username' => 'mose',
            'password' => 'kubusama1986',
            'charset' => 'utf8',
            'tablePrefix' => '',
            'initSQLs' => array("set time_zone='+00:00';"),
        ),
        'authManager' => array(
            'class' => 'RDbAuthManager',
            'connectionID' => 'db',
            'itemTable' => 'yiiright_authitem',
            'itemChildTable' => 'yiiright_authitemchild',
            'assignmentTable' => 'yiiright_authassignment',
            'rightsTable' => 'yiiright_rights',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'request' => array(
            'enableCookieValidation' => true,
            'enableCsrfValidation' => false,
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'rules' => array(
                'post/<id:\d+>/<title:.*?>' => 'post/view',
                'posts/<tag:.*?>' => 'post/index',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                // debug toolbar configuration
                array(
                    'class' => 'XWebDebugRouter',
                    'config' => 'alignLeft, opaque, runInDebug, fixedPos, collapsed, yamlStyle',
                    'levels' => 'error, warning, trace, profile, info',
                    'allowedIPs' => array('127.0.0.1'),
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),
        'bootstrap' => array(
            'class' => 'ext.bootstrap.components.Bootstrap',
            'responsiveCss' => true,

        ),
        'par' => array(
            'class' => 'application.extensions.dbparam.XDbParam',
            'connectionID' => 'db', //id of the connection component, just the same as with CDbCache
//  'preload' => 'test,test2', //comma-separated string or array of params to be loaded anyway. Other params are loaded only when requested.
//  'autoLoad' => true,//loads all attributes when initializing
//  'caseSensitive' => true, //setting to true makes all parameters case sensitive
        ),
        'ePdf' => array(
            'class' => 'ext.yii-pdf.EYiiPdf',
            'params' => array(
                'mpdf' => array(
                    'librarySourcePath' => 'application.vendors.mpdf57.*',
                    'constants' => array(
                        '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                    ),
                    'class' => 'mpdf', // the literal class filename to be loaded from the vendors folder
                    /*'defaultParams'     => array( // More info: http://mpdf1.com/manual/index.php?tid=184
                        'mode'              => '', //  This parameter specifies the mode of the new document.
                        'format'            => 'A4', // format A4, A5, ...
                        'default_font_size' => 0, // Sets the default document font size in points (pt)
                        'default_font'      => '', // Sets the default font-family for the new document.
                        'mgl'               => 15, // margin_left. Sets the page margins for the new document.
                        'mgr'               => 15, // margin_right
                        'mgt'               => 16, // margin_top
                        'mgb'               => 16, // margin_bottom
                        'mgh'               => 9, // margin_header
                        'mgf'               => 9, // margin_footer
                        'orientation'       => 'P', // landscape or portrait orientation
                    )*/
                ),
                'HTML2PDF' => array(
                    'librarySourcePath' => 'application.vendors.html2pdf.*',
                    'classFile' => 'html2pdf.class.php', // For adding to Yii::$classMap
                    /*'defaultParams'     => array( // More info: http://wiki.spipu.net/doku.php?id=html2pdf:en:v4:accueil
                        'orientation' => 'P', // landscape or portrait orientation
                        'format'      => 'A4', // format A4, A5, ...
                        'language'    => 'en', // language: fr, en, it ...
                        'unicode'     => true, // TRUE means clustering the input text IS unicode (default = true)
                        'encoding'    => 'UTF-8', // charset encoding; Default is UTF-8
                        'marges'      => array(5, 5, 5, 8), // margins by default, in order (left, top, right, bottom)
                    )*/
                )
            ),
        ),
    ),


    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        require(dirname(__FILE__) . '/params.php'),
        'languages' => array(
            'ms' => 'Bahasa Malaysia',
            'en' => 'English',
        ),),
);
