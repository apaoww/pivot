<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return array(
    "Al-Baghdadi Student Information Systems"=>"Sistem Maklumat Pelajar Al-Baghdadi",
    "Welcome "=>"Selamat datang ke ",
    'Login'=>'Log Masuk',
    'Home'=>'Laman Utama',
    'Logout'=>'Log Keluar',
    'Copyright'=>'Hakcipta',
    'by'=>'oleh',
    'Please fill out the following form with your login credentials:'=>
    'Sila isikan maklumat log masuk:',
    'Fields with '=>'Medan bertanda ',
    ' are required'=>' wajib diisi',
    'Username'=>'Id Penguna',
	'Operations'=>'Operasi',
    'Create ' => 'Tambah ',
    'Create' => 'Tambah',
    'Save' => 'Simpan',
    'View '=> 'Papar ',
    'View' =>'Papar',
    'Update '=>'Kemaskini ',
    'Update' =>'Kemaskini',
    'Manage '=>'Urus ',
    'Manage'=>'Urus',
    'List '=>'Senarai ',
    'List'=>'Senarai',
    'Delete' =>'Hapus',
    'Delete '=>'Hapus ',
    'Are you sure you want to delete this item?'=>
    'Anda pasti untuk hapus item ini?',
    'Valid login account a required to use this system.'=>
    'Akaun log masuk yang sah di perlukan untuk mengunakan sistem ini.',
    'User Setting'=>'Menu Penguna Semasa',
    'Create New Profile'=>'Lengkapkan Maklumat Penguna',
    'Administrator Menu'=>'Menu Administator',
    'Company Structure'=>'Struktur Organisasi',
    'Advanced Search' => 'Carian Lebih Detail',
    'Job Title' => 'Jenis Jawatan',
    'Leave Type'=>'Jenis Cuti',
    'Pay Grade'=>'Gred Gaji',
    'Qualification Type'=>'Jenis Kelulusan',
    'Operations'=>'Menu',
    'Manage User'=>'Urus Penguna',
    'Personal Informations'=>'Maklumat Peribadi',
    "Register user"=>"Daftar penguna",
    "Type number here"=>"Taip no telefon disini",
    'Send '=>'Hantar ',
    'Add Contacts '=>'Tambah Penerima ',
    'Double click on the table row to add or remove number'=>
    'Klik sebanyak dua kali pada baris senarai nama untuk tambah atau alih nombor',
    'Send'=>'Hantar',
    'Resend'=>'Hantar semula',
    'Register '=>'Daftar ',
    ' are required.'=>' wajib diisi.',
    "A. Personal Information"=>"A. Maklumat Peribadi",
    "Single"=>"Bujang",
    "Married"=>"Berkahwin",
    'Click to open calendar'=>'Klik untuk buka kalendar',
    "B. Academic Information"=>"B. Maklumat Akademik",
    "C. Teaching Experience in Quran"=>"C. Pengalaman Mengajar Al-Quran",
    'Recent Activity '=>'Aktiviti Terkini ',
);
?>
