<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$nameColumn = $this->guessNameColumn($this->tableSchema->columns);
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	Yii::t('modelLabels', '$label')=>array('index'),
	\$model->{$nameColumn},
);\n";
?>

$this->menu=array(
array('label'=>Yii::t('strings', 'List ').<?php echo "Yii::t('modelLabels', '".$this->modelClass."')"; ?>,'url'=>array('index')),
array('label'=>Yii::t('strings', 'Create ').<?php echo "Yii::t('modelLabels', '".$this->modelClass."')"; ?>,'url'=>array('create')),
array('label'=>Yii::t('strings', 'Update ').<?php echo "Yii::t('modelLabels', '".$this->modelClass."')"; ?>,'url'=>array('update','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>)),
array('label'=>Yii::t('strings', 'Delete ').<?php echo "Yii::t('modelLabels', '".$this->modelClass."')"; ?>,'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>),'confirm'=>Yii::t('strings', 'Are you sure you want to delete this item?'))),
array('label'=>Yii::t('strings', 'Manage ').<?php echo "Yii::t('modelLabels', '".$this->modelClass."')"; ?>,'url'=>array('admin')),
);
?>

<h1> <?php echo "<?php echo Yii::t('strings', 'View ');?>";?><?php echo "<?php echo Yii::t('modelLabels', '".$this->modelClass . "');?> #<?php echo \$model->{$this->tableSchema->primaryKey}; ?>"; ?></h1>

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
<?php
foreach ($this->tableSchema->columns as $column) {
	echo "\t\t'" . $column->name . "',\n";
}
?>
),
)); ?>
