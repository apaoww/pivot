<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	Yii::t('modelLabels', '$label')=>array('index'),
	Yii::t('strings', 'Manage'),
);\n";
?>

$this->menu=array(
array('label'=>Yii::t('strings', 'List ').<?php echo "Yii::t('modelLabels', '".$this->modelClass."')"; ?>,'url'=>array('index')),
array('label'=>Yii::t('strings', 'Create ').<?php echo "Yii::t('modelLabels', '".$this->modelClass."')"; ?>,'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('<?php echo $this->class2id($this->modelClass); ?>-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1><?php echo "<?php echo Yii::t('strings', 'Manage ')?>";?><?php echo "<?php echo Yii::t('modelLabels', '".$this->pluralize($this->class2name($this->modelClass))."'); ?>"; ?></h1>



<?php echo "<?php echo CHtml::link(Yii::t('strings', 'Advanced Search'),'#',array('class'=>'search-button btn')); ?>"; ?>

<div class="search-form" style="display:none">
	<?php echo "<?php \$this->renderPartial('_search',array(
	'model'=>\$model,
)); ?>\n"; ?>
</div><!-- search-form -->

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'<?php echo $this->class2id($this->modelClass); ?>-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
<?php
$count = 0;
foreach ($this->tableSchema->columns as $column) {
	if (++$count == 7) {
		echo "\t\t/*\n";
	}
	echo "\t\t'" . $column->name . "',\n";
}
if ($count >= 7) {
	echo "\t\t*/\n";
}
?>
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
